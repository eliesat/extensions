#!/bin/sh
if [ -d /media/hdd ]; then
dir=/media/hdd
elif [ -d /media/usb ]; then
dir=/media/usb
elif [ -d /media/mmc ]; then
dir=/media/mmc
else
echo""
fi

if [ -d /usr/lib/enigma2/python/Plugins/Extensions/BootLogoSwapper ]; then
echo "> removing package please wait..."
sleep 3s 
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/BootLogoSwapper

if [ -d $dir/Eliesat_Bootlogos ]; then
rm -rf $dir/Eliesat_Bootlogos >/dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3s

else

#download install plugin
if [ ! -d '/usr/lib/enigma2/python/Plugins/Extensions/BootLogoSwapper' ]; then
wget -O /var/volatile/tmp/$package.tar.gz --no-check-certificate https://gitlab.com/eliesat/extensions/-/raw/main/bootlogoswapper/bootlogo-swapper.tar.gz
tar -xzf /tmp/$package.tar.gz -C /
rm -rf /tmp/$package.tar.gz >/dev/null 2>&1
fi
######################################


#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1
#remove bootlogos
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/BootLogoSwapper/bootlogos/*.mvi >/dev/null 2>&1
rm -rf $dir/Eliesat_Bootlogos >/dev/null 2>&1
rm -rf $dir/bootlogos >/dev/null 2>&1

#config
plugin=bootlogos-eliesat
version=special-edition-1.0
url=https://gitlab.com/eliesat/extensions/-/raw/main/bootlogoswapper/bootlogos-eliesat-special-edition-1.0.tar.gz
package=/var/volatile/tmp/$plugin-$version.tar.gz

#download & install
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3s

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf $package >/dev/null 2>&1

if [ -d $dir ]; then
if [ ! -d $dir/Eliesat_Bootlogos ]; then
mkdir /$dir/Eliesat_Bootlogos

mv /usr/lib/enigma2/python/Plugins/Extensions/BootLogoSwapper/bootlogos/*.mvi $dir/Eliesat_Bootlogos/
fi
ln -s $dir/Eliesat_Bootlogos/* /usr/lib/enigma2/python/Plugins/Extensions/BootLogoSwapper/bootlogos
fi

echo ''
if [ $extract -eq 0 ]; then
echo "> $plugin-$version package installed successfully"
echo "> Uploaded By ElieSat"
sleep 3s

else

echo "> $plugin-$version package installation failed"
sleep 3s
fi

fi
exit 0
