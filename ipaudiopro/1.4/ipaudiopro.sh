#!/bin/bash

#configuration
###########################################

package="enigma2-plugin-extensions-ipaudiopro"
version="1.4"
#aarch64 armv7ahf-neon armv7athf-neon armv7a-neon armv7a cortexa15hf-neon-vfpv4 cortexa9hf-neon mips32el sh4

PY=python
[[ -e /usr/bin/python3 ]] && PY=python3
export D=${D}

get_oever() {
	OEVER=$($PY - <<END
import sys
sys.path.append('/usr/lib/enigma2/python')
try:
	from boxbranding import getOEVersion
	oever = getOEVersion()
	print(oever)
except:
	print("unknown")
END
	)
	OEVER=$(echo $OEVER | sed "s/OE-Alliance //")
	if [ "x$OEVER" == "xunknown" ]; then
		if [[ -x "/usr/bin/openssl" ]]; then
			SSLVER=$(openssl version | awk '{ print $2 }')
			case "$SSLVER" in
				1.0.2a|1.0.2b|1.0.2c|1.0.2d|1.0.2e|1.0.2f)
					OEVER="unknown"
					;;
				1.0.2g|1.0.2h|1.0.2i)
					OEVER="3.4"
					;;
				1.0.2j)
					OEVER="4.0"
					;;
				1.0.2k|1.0.2l)
					OEVER="4.1"
					;;
				1.0.2m|1.0.2n|1.0.2o|1.0.2p)
					OEVER="4.2"
					;;
				1.0.2q|1.0.2r|1.0.2s)
					OEVER="4.3"
					;;
				*)
					OEVER="unknown"
					;;
			esac
		fi
	fi
}

get_arch() {
	ARCH=$($PY - <<END
import sys
sys.path.append('/usr/lib/enigma2/python')
try:
	from boxbranding import getImageArch
	arch = getImageArch()
	print(arch)
except:
	print("unknown")
END
	)
	if [ "x$ARCH" == "xunknown" ]; then
		case "$OEVER" in
			3.4|4.0)
				ARCH="armv7a-neon"
				;;
			4.1)
				ARCH="armv7athf-neon"
				;;
			*)
				ARCH="armv7a"
				;;
		esac
		echo $(uname -m) | grep -q "aarch64" && ARCH="aarch64"
		echo $(uname -m) | grep -q "mips" && ARCH="mips32el"
		echo $(uname -m) | grep -q "sh4" && ARCH="sh4"
		if echo $(uname -m) | grep -q "armv7l"; then
			echo $(cat /proc/cpuinfo | grep "CPU part" | uniq) | grep -q "0xc09" && ARCH="cortexa9hf-neon"
			if echo $(cat /proc/cpuinfo | grep "CPU part" | uniq) | grep -q "0x00f"; then
				case "$OEVER" in
					3.4)
						ARCH="armv7ahf-neon"
						;;
					*)
						ARCH="cortexa15hf-neon-vfpv4"
						;;
				esac
			fi
		fi
	fi
}

check_compat() {
	case "$OEVER" in
		unknown)
			echo Broken boxbranding ...
			exit 1
			;;
		3.4)
			;;
		3.*)
			echo Your image is EOL ...
			exit 1
			;;
		2.*)
			echo Your image is EOL ...
			exit 1
			;;
		1.*)
			echo Your image is EOL ...
			exit 1
			;;
		0.*)
			echo Your image is EOL ...
			exit 1
			;;
	esac
	if [ "x$ARCH" == "xunknown" ]; then
		echo Broken boxbranding ...
		exit 1
	fi
}

get_oever
get_arch
check_compat

#check python version
python=$(python -c "import platform; print(platform.python_version())")
sleep 1;
case $python in 
2.*)
ff="ff4_4_3"
pp="py2_7"
;;
3.9.*)
ff="ff4_4_3"
pp="py3_9"
;;
3.11.*)
ff="ff6_1"
pp="py3_11"
;;
3.12.[1-5])
ff="ff6_1"
pp="py3_12"
;;
3.12.[6-9])
ff="ff7_1"
pp="py3_12"
;;
3.13.*)
ff="ff7_1"
pp="py3_13"
;;
*)
echo "> your image python version: $python is not supported"
sleep 3
exit 1
;;
esac

plugin="${package}_${version}_${ARCH}_${pp}_${ff}"
site="https://gitlab.com/eliesat/extensions/-/raw/main/ipaudiopro/1.4"
temp_dir="/tmp"


print_message() {
    echo "[$(date +'%Y-%m-%d %H:%M:%S')] $1"
}

# Determine package manager and plugin extension
###########################################
if command -v dpkg &> /dev/null; then
    package_manager="apt"
    install_command="dpkg -i --force-overwrite"
    uninstall_command="apt-get purge --auto-remove -y"
    status_file="/var/lib/dpkg/status"
    plugin_extension="deb"
else
    package_manager="opkg"
    install_command="opkg install --force-reinstall"
    uninstall_command="opkg remove --force-depends"
    status_file="/var/lib/opkg/status"
    plugin_extension="ipk"
fi

#determine and check url
###########################################
url="$site/$plugin.$plugin_extension"
if wget -q --method=HEAD $url; then
  pluginextension=found
else
  print_message "> $plugin.$plugin_extension not found"
  print_message "> your device is not supported"
exit 1
fi

# Functions
###########################################
cleanup() {
    print_message "> Performing cleanup..."
    [ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
    rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
    print_message "> Cleanup completed."
}

#check andd install or remove package
###########################################
check_and_install_package() {

plugin_path="/usr/lib/enigma2/python/Plugins/Extensions/IPaudioPro"
if [ -d $plugin_path ]; then
    print_message "> removing package old version please wait..."
    sleep 3 
    rm -rf $plugin_path > /dev/null 2>&1

    if grep -q "$plugin.$plugin_extension" "$status_file"; then
        $uninstall_command $plugin.$plugin_extension > /dev/null 2>&1
    fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
fi
    print_message "> Downloading $plugin.$plugin_extension, please wait..."
    wget -q --show-progress $url -P "$temp_dir"
    if [ $? -ne 0 ]; then
        print_message "> Failed to download $plugin.$plugin_extension from $url"
        exit 1
    fi
sleep 3
    print_message "> Installing $plugin.$plugin_extension, please wait..."
    $install_command "$temp_dir/$plugin.$plugin_extension"
    if [ $? -eq 0 ]; then
    wget -qO $plugin_path/logo.png $site/logo.png
        print_message "> $plugin.$plugin_extension installed successfully."
    else
        print_message "> Installation failed."
        exit 1
    fi
}

# Main
trap cleanup EXIT
check_and_install_package
