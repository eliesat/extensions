#!/bin/sh

# Configure ajpanel_menu.xml path
espp=$(grep config.plugins.AJPanel.backupPath /etc/enigma2/settings | cut -d '=' -f 2)
ajpanel_menu="ajpanel_menu.xml"
ajpanel_menu_path="$espp$ajpanel_menu"

#determine local panel version
lpv=$(sed -n '/About/p' $ajpanel_menu_path | sed 's/^.*">/">/' | sed 's/">*//' | sed 's/<.*//') 

#determine web panel version
wget -qO  /tmp/version.txt --no-check-certificate https://gitlab.com/eliesat/extensions/-/raw/main/ajpanel/version.txt |
sleep 3
wpv=$(sed -n '/20/p' /tmp/version.txt)

#update panel
if [ $lpv == $wpv ]; then
panel=updated
else 
wget "http://localhost/web/message?text=> Eliesatpanel,new update available please wait...&type=5&timeout=5" >/dev/null 2>&1
sleep 5
wget -q "--no-check-certificate" https://gitlab.com/eliesat/extensions/-/raw/main/ajpanel/eliesatpanel/update.sh -O - | /bin/sh
fi