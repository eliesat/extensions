#!/bin/sh

# Configuration
#########################################
plugin="iptvarchive"
git_url="https://gitlab.com/eliesat/extensions/-/raw/main/iptvarchive"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="/usr/lib/enigma2/python/Plugins/Extensions/IPTVarchive"
package="enigma2-plugin-extensions-$plugin"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
rm -rf $plugin_path > /dev/null 2>&1

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
  echo "> $plugin-$version package installed successfully"
if [ -f /etc/image-version ]; then
    image=$(cat /etc/image-version | grep -iF "creator" | cut -d"=" -f2 | xargs)
elif [ -f /etc/issue ]; then
    image=$(cat /etc/issue | head -n1 | awk '{print $1;}')
else
    image=''
fi
[[ ! -z "$image" ]] && echo -e "$image image detected"

PluginName="IPTVarchive"
pyver=$(python -V 2>&1 | cut -d\  -f2 | awk -F "." '{print $1$2}')
path="/usr/lib/enigma2/python"
destination="$path/Plugins/Extensions/$PluginName"
link="$path/Components/Renderer/_RunningText.py"
cp -a $destination/$pyver/. $destination/ > /dev/null 2>&1
if [ ! -L $link ]; then
    ln -s $destination/_RunningText.py $link > /dev/null 2>&1
fi
if [ $? -eq 0 ];then
    echo $'27\n38\n39\n310\n311\n312\n' | xargs -I{} rm -fr $destination/{}
    find . -name "enigma2-plugin-extensions-$PluginName_*.*" -type f -delete
fi
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
}
cleanup
    