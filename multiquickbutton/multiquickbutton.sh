#!/bin/sh

# Configuration
#########################################
plugin="multiquickbutton"
git_url="https://gitlab.com/eliesat/extensions/-/raw/main/multiquickbutton"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="/usr/lib/enigma2/python/Plugins/Extensions/MultiQuickButton"
package="enigma2-plugin-extensions-$plugin"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
rm -rf $plugin_path > /dev/null 2>&1
sed -ie s!"<\!\-\- <key id=\"KEY_RED\" mapto=\"timeshiftActivateEnd\" flags=\"b\" /> \-\->"!"<key id=\"KEY_RED\" mapto=\"timeshiftActivateEnd\" flags=\"b\" />"!g "/usr/share/enigma2/keymap.xml"
sed -ie s!"<\!\-\- <key id=\"KEY_YELLOW\" mapto=\"timeshiftActivateEndAndPause\" flags=\"b\" /> \-\->"!"<key id=\"KEY_YELLOW\" mapto=\"timeshiftActivateEndAndPause\" flags=\"b\" />"!g "/usr/share/enigma2/keymap.xml"
rm -r /usr/lib/enigma2/python/Plugins/Extensions/MultiQuickButton > /dev/null 2>&1
rm -r /etc/MultiQuickButton > /dev/null 2>&1

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
if ! test -d /etc/MultiQuickButton
	then
		mkdir /etc/MultiQuickButton
fi

cd /tmp/mqb
for buttonfile in *.xml
do
	if ! test -f /etc/MultiQuickButton/$buttonfile
		then
			cp /tmp/mqb/$buttonfile /etc/MultiQuickButton
	fi
done

cd /
rm -rf /tmp/mqb
echo "Backup /usr/share/enigma2/keymap.xml in /usr/share/enigma2/keymap_backup_mqb.xml"
cp /usr/share/enigma2/keymap.xml /usr/share/enigma2/keymap_backup_mqb.xml

echo "Setting flags in /usr/share/enigma2/keymap.xml ..."
sed -ie s!"<key id=\"KEY_YELLOW\" mapto=\"timeshiftStart\" flags=\"m\" />"!"<key id=\"KEY_YELLOW\" mapto=\"timeshiftStart\" flags=\"b\" />"!g "/usr/share/enigma2/keymap.xml"
sed -ie s!"<key id=\"KEY_YELLOW\" mapto=\"timeshiftActivateEndAndPause\" flags=\"m\" />"!"<key id=\"KEY_YELLOW\" mapto=\"timeshiftActivateEndAndPause\" flags=\"b\" />"!g "/usr/share/enigma2/keymap.xml"
sed -ie s!"<key id=\"KEY_VIDEO\" mapto=\"showMovies\" flags=\"m\" />"!"<key id=\"KEY_VIDEO\" mapto=\"showMovies\" flags=\"b\" />"!g "/usr/share/enigma2/keymap.xml"
sed -ie s!"<key id=\"KEY_RADIO\" mapto=\"showRadio\" flags=\"m\" />"!"<key id=\"KEY_RADIO\" mapto=\"showRadio\" flags=\"b\" />"!g "/usr/share/enigma2/keymap.xml"
sed -ie s!"<key id=\"KEY_TEXT\" mapto=\"startTeletext\" flags=\"m\" />"!"<key id=\"KEY_TEXT\" mapto=\"startTeletext\" flags=\"b\" />"!g "/usr/share/enigma2/keymap.xml"
sed -ie s!"<key id=\"KEY_HELP\" mapto=\"displayHelp\" flags=\"m\" />"!"<key id=\"KEY_HELP\" mapto=\"displayHelp\" flags=\"b\" />"!g "/usr/share/enigma2/keymap.xml"

#echo "... deactivating timeshift buttons YELLOW/RED not needed for VU+"
#grep -q '<!-- <key id="KEY_RED" mapto="timeshiftActivateEnd" flags="b" /> -->' /usr/share/enigma2/keymap.xml
#if [ $? -eq 0 ]
#    then
#	echo "timeshift button RED already deactivated"
#    else
#	sed -ie s!"<key id=\"KEY_RED\" mapto=\"timeshiftActivateEnd\" flags=\"b\" />"!"<\!\-\- <key id=\"KEY_RED\" mapto=\"timeshiftActivateEnd\" flags=\"b\" /> \-\->"!g "/usr/share/enigma2/keymap.xml"
#fi

#grep -q '<!-- <key id="KEY_YELLOW" mapto="timeshiftActivateEndAndPause" flags="b" /> -->' /usr/share/enigma2/keymap.xml
#if [ $? -eq 0 ]
#    then
#	echo "timeshift button YELLOW already deactivated"
#    else
#	sed -ie s!"<key id=\"KEY_YELLOW\" mapto=\"timeshiftActivateEndAndPause\" flags=\"b\" />"!"<\!\-\- <key id=\"KEY_YELLOW\" mapto=\"timeshiftActivateEndAndPause\" flags=\"b\" /> \-\->"!g "/usr/share/enigma2/keymap.xml"
#fi

grep -q '<!-- <key id="KEY_RED" mapto="activateRedButton" flags="b" /> -->' /usr/share/enigma2/keymap.xml
if [ $? -eq 0 ]
    then
	echo "RED button already deactivated"
    else
	sed -ie s!"<key id=\"KEY_RED\" mapto=\"activateRedButton\" flags=\"b\" />"!"<\!\-\- <key id=\"KEY_RED\" mapto=\"activateRedButton\" flags=\"b\" /> \-\->"!g "/usr/share/enigma2/keymap.xml"
fi

grep -q '<!-- <key id="KEY_GREEN" mapto="subserviceSelection" flags="b" /> -->' /usr/share/enigma2/keymap.xml
if [ $? -eq 0 ]
    then
	echo "GREEN button already deactivated"
    else
	sed -ie s!"<key id=\"KEY_GREEN\" mapto=\"subserviceSelection\" flags=\"b\" />"!"<\!\-\- <key id=\"KEY_GREEN\" mapto=\"subserviceSelection\" flags=\"b\" /> \-\->"!g "/usr/share/enigma2/keymap.xml"
fi

grep -q '<!-- <key id="KEY_YELLOW" mapto="subserviceSelection" flags="b" /> -->' /usr/share/enigma2/keymap.xml
if [ $? -eq 0 ]
    then
	echo "YELLOW button already deactivated"
    else
	sed -ie s!"<key id=\"KEY_YELLOW\" mapto=\"audioSelection\" flags=\"b\" />"!"<\!\-\- <key id=\"KEY_YELLOW\" mapto=\"audioSelection\" flags=\"b\" /> \-\->"!g "/usr/share/enigma2/keymap.xml"
fi

grep -q 'config.plugins.QuickButton.okexitstate=true' /etc/enigma2/settings
if [ $? -eq 0 ]
    then
	echo "Ok/Exit state found in /etc/enigma2/settings => activate ok/exit buttons"
	cp -f /usr/lib/enigma2/python/Plugins/Extensions/MultiQuickButton/keymap_ok.xml /usr/lib/enigma2/python/Plugins/Extensions/MultiQuickButton/keymap.xml
fi
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
}
cleanup
    