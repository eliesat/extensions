#!/bin/sh

# Configuration
plugin="neoboot"
version="8.20"
targz_file="$plugin-$version.tar.gz"
package="enigma2-plugin-extensions-neoboot"
temp_dir="/tmp"
url="https://gitlab.com/eliesat/extensions/-/raw/main/neoboot/neoboot-8.20.tar.gz"

# Determine package manager
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

check_and_remove_package() {
if [ -d /usr/lib/enigma2/python/Plugins/Extensions/NeoBoot ]; then
echo "> removing package old version please wait..."
sleep 3
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/NeoBoot > /dev/null 2>&1

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

# Remove unnecessary files and folders
[ -d "/CONTROL" ] && rm -r /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1

#download & install package
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C /
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
Cel="/usr/lib/enigma2/python/Plugins/Extensions"
cd /usr/lib/enigma2/python/Plugins/Extensions/NeoBoot/; chmod 755 ./bin/*;chmod 755 ./target/*; chmod 755 ./ex_init.py;chmod 755 ./files/*.sh; chmod 755 ./files/targetimage.sh; chmod 755 ./files/NeoBoot.sh; chmod 755 ./files/S50fat.sh; chmod 755 ./bin/rebootbot;chmod -R +x ./ubi_reader/*; cd;
chmod -R +x /usr/lib/enigma2/python/Plugins/Extensions/NeoBoot/ubi_reader/*
echo "> $plugin-$version package installed successfully"
sleep 3
echo "> Uploaded By ElieSat"
# Restart Enigma2 service or kill enigma2 based on the system
    if [ -f /etc/apt/apt.conf ]; then
    sleep 2
    systemctl restart enigma2
    else
    sleep 2
    killall -9 enigma2
    fi
else
echo "> $plugin-$version package installation failed"
sleep 3
fi
    