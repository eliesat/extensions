#!/bin/sh

# Configuration
plugin="mycam"
version="7.0"
targz_file="$plugin-$version.tar.gz"

#determine plugin url based on image version
python=$(python -c "import platform; print(platform.python_version())")

case $python in 
2.7.18)
url="https://gitlab.com/eliesat/extensions/-/raw/main/mycam/py2/mycam-7.0.tar.gz"
sleep 3
;;
3.11.0|3.11.1|3.11.2|3.11.3|3.11.4|3.11.5|3.11.6|3.9.7|3.9.9)
url="https://gitlab.com/eliesat/extensions/-/raw/main/mycam/py3/mycam-7.0.tar.gz"
sleep 3
;;
3.12.1|3.12.2|3.12.3|3.12.4|3.12.5|3.12.6)
url="https://gitlab.com/eliesat/extensions/-/raw/main/mycam/py312/mycam-7.0.tar.gz"
sleep 3
;;

*)
echo "> your image python version: $python is not supported"
sleep 3
exit 1
;;
esac



if [ -d /usr/lib/enigma2/python/Plugins/Extensions/mycam7 ]; then
echo "> removing package old version please wait..."
sleep 3 
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/mycam7 > /dev/null 2>&1

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
else

#download & install package
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget -O $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C /
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then
echo "> $plugin-$version package installed successfully"
sleep 3
echo "> Uploaded By ElieSat"
else
echo "> $plugin-$version package installation failed"
sleep 3
fi

fi