#!/bin/sh

# Configuration
#########################################
plugin="e2iplayer"
git_url="https://gitlab.com/eliesat/extensions/-/raw/main/e2iplayer-hungarian"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer"
package="enigma2-plugin-extensions-$plugin"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
rm -rf $plugin_path > /dev/null 2>&1

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

#dependencies
#########################################
arrVar=("enigma2-plugin-extensions-e2iplayer-deps" "exteplayer3" "gstplayer" "python3-sqlite3" "python3-pycurl" "python3-json" "python3-requests" "python3-requests-cache" "python3-pycryptodome")
if [ "$pyVersion" != 3 ]; then
imgpy=ok
else
echo -e "Sorry Plugin Do Not Support Python 2"
sleep 4
exit 1
fi
if [ -f /etc/opkg/opkg.conf ]; then
Status='/var/lib/opkg/status'
OStype='Opensource'
Update='opkg update'
OSinstall='opkg install'
elif [ -f /etc/apt/apt.conf ]; then
Status='/var/lib/dpkg/status'
OStype='DreamOS'
Update='apt-get update'
OSinstall='apt-get install --fix-broken --yes --assume-yes'
fi
case $(uname -m) in
armv7l*) plarform="armv7" ;;
mips*) plarform="mipsel" ;;
sh4*) plarform="sh4" ;;
aarch64*) plarform="aarch64" ;;
esac
$Update >/dev/null 2>&1
install() {
if ! grep -qs "Package: $1" "${Status}"; then
echo -e " >>>>   need to install $1 <<<<"
sleep 0.8
echo
${OSinstall} "$1"
sleep 0.8
clear
fi
}
clear

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
}
cleanup
    