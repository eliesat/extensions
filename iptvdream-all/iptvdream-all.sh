#!/bin/sh

# Configuration
#########################################
plugin="iptvdream-all"
git_url="https://gitlab.com/eliesat/extensions/-/raw/main/iptvdream-all"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="/usr/lib/enigma2/python/Plugins/Extensions/IPtvDream"
package="enigma2-plugin-extensions-$plugin"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
keymap=/usr/lib/enigma2/python/Plugins/Extensions/IPtvDream/keymap.xml
echo "remove keymap link ${keymap}"
rm -f ${keymap}
rm -rf $plugin_path > /dev/null 2>&1
rm -rf /usr/share/enigma2/IPtvDream* > /dev/null 2>&1
if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
keymap=/usr/lib/enigma2/python/Plugins/Extensions/IPtvDream/keymap.xml
style=`cat /etc/enigma2/settings |sed -n 's/^config.plugins.IPtvDream.keymap_type=\(.*\)/\1/p'`
echo "detected keymap '${style}'"
if test "${style}" = "neutrino"
then
    ln -s keymap_neutrino.xml ${keymap}
else
    ln -s keymap_enigma.xml ${keymap}
fi
echo "created keymap link to `readlink ${keymap}`"
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
}
cleanup
    