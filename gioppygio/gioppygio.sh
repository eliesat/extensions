#!/bin/sh

# Configuration
#########################################
plugin="gioppygio"
git_url="https://gitlab.com/eliesat/extensions/-/raw/main/gioppygio"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="/usr/lib/enigma2/python/Plugins/Extensions/GioppyGio"
package="enigma2-plugin-extensions-$plugin"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
rm -rf $plugin_path > /dev/null 2>&1

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then

if [ -f /tmp/Date ];then
  cp "/tmp/Date" /usr/lib/enigma2/python/Plugins/Extensions/GioppyGio/Moduli/Settings/ > /dev/null 2>&1
fi
if [ -f /tmp/Select ];then
  cp "/tmp/Select" /usr/lib/enigma2/python/Plugins/Extensions/GioppyGio/Moduli/Settings/ > /dev/null 2>&1
fi
if [ -f /tmp/send_settings ];then
  cp "/tmp/send_settings" /etc/enigma2/ > /dev/null 2>&1
fi
if [ -f /tmp/gioppygio_picons ];then
  cp "/tmp/gioppygio_picons" /etc/enigma2/ > /dev/null 2>&1
fi
rm -rf /tmp/gioppygio.ipk > /dev/null 2>&1
echo "config.plugins.epgimport.enabled=true" >> /etc/enigma2/settings;
echo "config.plugins.epgimport.longDescDays=5" >> /etc/enigma2/settings;
echo "config.plugins.epgimport.wakeup=6:0" >> /etc/enigma2/settings
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
}
cleanup
    