

#configuration
#########################################
TMPDIR='/tmp/'

PLUGINPATH='/usr/lib/enigma2/python/Plugins/Extensions/'

SETTINGS='/etc/enigma2/settings'

URL='https://mohamed_os.gitlab.io/e2iplayer/'

MY_PATH='/media/mmc/'

pyv=$(python -c "import sys; print(''.join(map(str, sys.version_info[:2])))")
FileTar="e2iplayer-py${pyv}.tar.gz"


pyVersion=$(python -c"from sys import version_info; print(version_info[0])")
VERSION=$(wget --no-check-certificate -q -O} - ${URL}update2/lastversion.php | awk 'NR==1')


#dependencies
#########################################
arrVar=("enigma2-plugin-extensions-e2iplayer-deps" "exteplayer3" "gstplayer" "python3-sqlite3" "python3-pycurl" "python3-json" "python3-requests" "python3-requests-cache" "python3-pycryptodome")
if [ "$pyVersion" != 3 ]; then
echo -e "Sorry Plugin$ Do Not Support Python 2"
sleep 4
exit 1
fi
if [ -f /etc/opkg/opkg.conf ]; then
Status='/var/lib/opkg/status'
OStype='Opensource'
Update='opkg update'
OSinstall='opkg install'
elif [ -f /etc/apt/apt.conf ]; then
Status='/var/lib/dpkg/status'
OStype='DreamOS'
Update='apt-get update'
OSinstall='apt-get install --fix-broken --yes --assume-yes'
fi
case $(uname -m) in
armv7l*) plarform="armv7" ;;
mips*) plarform="mipsel" ;;
sh4*) plarform="sh4" ;;
aarch64*) plarform="aarch64" ;;
esac
$Update >/dev/null 2>&1
install() {
if ! grep -qs "Package: $1" "${Status}"; then
echo -e " >>>>   need to install $1 <<<<"
sleep 0.8
echo
${OSinstall} "$1"
sleep 0.8
clear
fi
}
clear

#download and install plugin
#########################################
if [ -e ${TMPDIR}"${FileTar}" ]; then
echo -e "remove archive file"
rm -f ${TMPDIR}"${FileTar}"
fi
if [ -e ${TMPDIR}e2iplayer-main ]; then
rm -fr ${TMPDIR}e2iplayer-main
fi
echo -e "Downloading e2iPlayer plugin Please Wait..."

wget --show-progress --no-check-certificate -qO "${TMPDIR}${FileTar}" "${URL}/${FileTar}"
wait
if ! [ -e ${TMPDIR}"${FileTar}" ]; then
echo -e "error downloading archive, end"
exit 1
else
echo -e ""
fi
tar -xzf ${TMPDIR}"${FileTar}" -C ${TMPDIR}
if ! [ -e "${TMPDIR}e2iplayer-main" ]; then
echo -e "> error extracting archive, end"
exit 1
else
echo -e ""
rm -f ${TMPDIR}"${FileTar}"
fi

if [ -e "${PLUGINPATH}IPTVPlayer" ]; then
if [ -e "${PLUGINPATH}IPTVPlayer/libs/iptvsubparser/_subparser.so" ]; then
cp -rf "${PLUGINPATH}IPTVPlayer/libs/iptvsubparser/_subparser.so" ${TMPDIR}
fi
if [ -e "${PLUGINPATH}IPTVPlayer/libs/e2icjson/e2icjson.so" ]; then
cp -rf "${PLUGINPATH}IPTVPlayer/libs/e2icjson/e2icjson.so" ${TMPDIR}
fi

rm -rf ${PLUGINPATH}IPTVPlayer
fi
cp -rf ${TMPDIR}e2iplayer-main/IPTVPlayer ${PLUGINPATH}
if [ -e "${TMPDIR}_subparser.so" ]; then
cp -rf ${TMPDIR}_subparser.so "${PLUGINPATH}IPTVPlayer/libs/iptvsubparser/"
fi
if [ -e "${TMPDIR}e2icjson.so" ]; then
cp -rf ${TMPDIR}e2icjson.so "${PLUGINPATH}IPTVPlayer/libs/e2icjso/"
fi
if ! [ -e "${PLUGINPATH}IPTVPlayer" ]; then
echo -e "error installing e2iplayer"
exit 1
else
echo -e ""
rm -fr ${TMPDIR}e2iplayer-main
fi
sleep 3
for i in "${arrVar[@]}"; do
install "$i"
done


pyv=$(python -c "import sys; print('.'.join(map(str, sys.version_info[:2])))")
if [ "$(find /usr/lib/python"$pyv"/site-packages/ | grep -Fic pycurl)" = 0 ]; then
wget --no-check-certificate -q -O - "${URL}resources/pycurlinstall.py" | python
fi
for FileBin in 'duk' 'cmdwrap' 'f4mdump' 'hlsdl' 'rtmpdump' 'uchardet'; do
if ! [ -e /usr/bin/${FileBin} ]; then
wget --no-check-certificate -q -O "/usr/bin/${FileBin}" "${URL}/resources/bin/${plarform}/${FileBin}"
chmod -R 755 /usr/bin/${FileBin}
fi
done


#settings
#########################################
sleep 3
if [ -e "${PLUGINPATH}IPTVPlayer" ]; then
echo "> applying settings..."
sleep 3
echo "> your device will restart now please wait..."
init 4
sleep 5
sed -e s/config.plugins.iptvplayer.*//g -i ${SETTINGS}
sleep 2
if [ -e "${MY_PATH}settings.sh" ]; then
{
echo "config.plugins.iptvplayer.AktualizacjaWmenu=true"
echo "config.plugins.iptvplayer.alternative${plarform^^}MoviePlayer=extgstplayer"
echo "config.plugins.iptvplayer.alternative${plarform^^}MoviePlayer0=extgstplayer"
echo "config.plugins.iptvplayer.buforowanie_m3u8=false"
echo "config.plugins.iptvplayer.cmdwrappath=/usr/bin/cmdwrap"
echo "config.plugins.iptvplayer.debugprint=/tmp/iptv.dbg"
echo "config.plugins.iptvplayer.default${plarform^^}MoviePlayer=exteplayer"
echo "config.plugins.iptvplayer.default${plarform^^}MoviePlayer0=exteplayer"
echo "config.plugins.iptvplayer.extplayer_infobanner_clockformat=24"
echo "config.plugins.iptvplayer.extplayer_skin=line"
echo "config.plugins.iptvplayer.extplayer_subtitle_background=transparent"
echo "config.plugins.iptvplayer.extplayer_subtitle_border_width=1"
echo "config.plugins.iptvplayer.extplayer_subtitle_font_size=45"
echo "config.plugins.iptvplayer.extplayer_subtitle_line_height=65"
echo "config.plugins.iptvplayer.f4mdumppath=/usr/bin/f4mdump"
echo "config.plugins.iptvplayer.gstplayerpath=/usr/bin/gstplayer"
echo "config.plugins.iptvplayer.ipaudio=False"
echo "config.plugins.iptvplayer.hlsdlpath=/usr/bin/hlsdl"
echo "config.plugins.iptvplayer.NaszaSciezka=${MY_PATH}Player/"
echo "config.plugins.iptvplayer.opensuborg_login=MOHAMED_OS"
echo "config.plugins.iptvplayer.opensuborg_password=&ghost@mcee2017&"
echo "config.plugins.iptvplayer.osk_type=system"
echo "config.plugins.iptvplayer.plarform=${plarform}"
echo "config.plugins.iptvplayer.remember_last_position=true"
echo "config.plugins.iptvplayer.rtmpdumppath=/usr/bin/rtmpdump"
echo "config.plugins.iptvplayer.SciezkaCache=${MY_PATH}IPTVCache/"
echo "config.plugins.iptvplayer.Sciezkaurllist=${MY_PATH}File/"
echo "config.plugins.iptvplayer.uchardetpath=/usr/bin/uchardet"
echo "config.plugins.iptvplayer.updateLastCheckedVersion=${VERSION}"
echo "config.plugins.iptvplayer.usepycurl=True"
echo "config.plugins.iptvplayer.watched_item_color=#FF0000"
echo "config.plugins.iptvplayer.wgetpath=wget"
echo "config.plugins.iptvplayer.xtreamPath=${MY_PATH}File/"
echo "config.plugins.iptvplayer.ytDefaultformat=9999"
echo "config.plugins.iptvplayer.ytUseDF=False"
} >>${SETTINGS}
else
{
echo "config.plugins.iptvplayer.skin=Adam1080p
echo" "config.plugins.iptvplayer.AktualizacjaWmenu=true"
echo "config.plugins.iptvplayer.alternative${plarform^^}MoviePlayer=extgstplayer"
echo "config.plugins.iptvplayer.alternative${plarform^^}MoviePlayer0=extgstplayer"
echo "config.plugins.iptvplayer.buforowanie_m3u8=false"
echo "config.plugins.iptvplayer.cmdwrappath=/usr/bin/cmdwrap"
echo "config.plugins.iptvplayer.debugprint=/tmp/iptv.dbg"
echo "config.plugins.iptvplayer.default${plarform^^}MoviePlayer=exteplayer"
echo "config.plugins.iptvplayer.default${plarform^^}MoviePlayer0=exteplayer"
echo "config.plugins.iptvplayer.extplayer_infobanner_clockformat=24"
echo "config.plugins.iptvplayer.extplayer_skin=line"
echo "config.plugins.iptvplayer.extplayer_subtitle_background=transparent"
echo "config.plugins.iptvplayer.extplayer_subtitle_border_width=1"
echo "config.plugins.iptvplayer.extplayer_subtitle_font_size=45"
echo "config.plugins.iptvplayer.extplayer_subtitle_line_height=65"
echo "config.plugins.iptvplayer.f4mdumppath=/usr/bin/f4mdump"
echo "config.plugins.iptvplayer.gstplayerpath=/usr/bin/gstplayer"
echo "config.plugins.iptvplayer.hlsdlpath=/usr/bin/hlsdl"
echo "config.plugins.iptvplayer.opensuborg_login=MOHAMED_OS"
echo "config.plugins.iptvplayer.opensuborg_password=&ghost@mcee2017&"
echo "config.plugins.iptvplayer.osk_type=system"
echo "config.plugins.iptvplayer.plarform=${plarform}"
echo "config.plugins.iptvplayer.remember_last_position=true"
echo "config.plugins.iptvplayer.rtmpdumppath=/usr/bin/rtmpdump"
echo "config.plugins.iptvplayer.uchardetpath=/usr/bin/uchardet"
echo "config.plugins.iptvplayer.updateLastCheckedVersion=${VERSION}"
echo "config.plugins.iptvplayer.usepycurl=True"
echo "config.plugins.iptvplayer.watched_item_color=#FF0000"
echo "config.plugins.iptvplayer.wgetpath=wget"
echo "config.plugins.iptvplayer.ytDefaultformat=9999"
echo "config.plugins.iptvplayer.ytUseDF=False"
} >>${SETTINGS}
fi
fi
echo "> Script by:MOHAMED_OS ..."
sleep 0.8
echo -e "> Your device will restart now..."
if [ "${OStype}" = "DreamOS" ]; then
sleep 2
reboot -f
else
init 4
sleep 2
init 3
fi
