#!/bin/sh

if mountpoint -q /usr/lib/enigma2/python/Plugins/Extensions/OpenMultiboot; then
    echo "openMultiBoot will only install on main image."
    echo "Child image is running - canceling installation!"
    sleep 3
    exit 1
else
    echo "Main image is running - proceeding installation..."
    exit 0
fi

# Configuration
#########################################
plugin="openmultiboot"
git_url="https://gitlab.com/eliesat/extensions/-/raw/main/openmultiboot"
version="2.6"
plugin_path="/usr/lib/enigma2/python/Plugins/Extensions/OpenMultiboot"
package="enigma2-plugin-extensions-$plugin"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
if mountpoint -q /usr/lib/enigma2/python/Plugins/Extensions/OpenMultiboot; then
    echo "openMultiBoot will only remove on main image."
    exit 0
else
    echo "Main image is running - proceeding removing..."
fi

rm /sbin/init 2>/dev/null
ln -s /sbin/init.sysvinit /sbin/init 2>/dev/null
rm -rf /sbin/open-multiboot-branding-helper.py 2>/dev/null

sleep 2

chown -Rh root:root /usr/lib/enigma2/python/Plugins/Extensions/OpenMultiboot 2>/dev/null
rm -rf $plugin_path > /dev/null 2>&1

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

#check & install dependencies
#########################################
check_and_install_dependencies() {
# Determine package manager
if command -v dpkg &> /dev/null; then
    install_command="apt-get install"
else
    install_command="opkg install"
fi

#check python version
python=$(python -c"from sys import version_info; print(version_info[0])")
sleep 1;
case $python in 
2)
$install_command kernel-module-nandsim kernel-module-block2mtd mtd-utils-ubifs python-subprocess mtd-utils unjffs2 libfreetype6 libc6 > /dev/null 2>&1
;;
3)
$install_command kernel-module-nandsim kernel-module-block2mtd mtd-utils-ubifs python3-subprocess mtd-utils unjffs2 libfreetype6 libc6 > /dev/null 2>&1
;;
*)
echo "> your image python version: $python is not supported"
sleep 3
exit 1
;;
esac
}
check_and_install_dependencies

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
}
cleanup
    