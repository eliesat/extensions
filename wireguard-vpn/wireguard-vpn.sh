#!/bin/sh

# Configuration
#########################################
plugin="wireguard-vpn"
git_url="https://gitlab.com/eliesat/extensions/-/raw/main/wireguard-vpn"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="/usr/lib/enigma2/python/Plugins/Extensions/Wireguard"
package="enigma2-plugin-extensions-$plugin"
temp_dir="/tmp"

#check arch armv7l aarch64 mips 7401c0 sh4
arch=$(uname -m)
#check python version
python_version=$(python -c "import platform; print(platform.python_version())")
echo "> python version: $python_version "
echo "> arch=: $arch "
sleep 3


if [ "$arch" == "armv7l" ]; then
case $python_version in 
3.12.*|3.13*)
targz_file="$plugin-arm.tar.gz";;
*)
echo "> your image python version: $python_version is not supported"
sleep 3
exit 1
;;
esac

elif [ "$arch" == "mips" ]; then
case $python_version in 
3.12.*|3.13*)
targz_file="$plugin-mips.tar.gz";;
*)
echo "> your image python version: $python_version is not supported"
sleep 3
exit 1
;;
esac

else
echo "> your device is not supported"
sleep 3
exit 1
fi
url="$git_url/$targz_file"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
rm -rf $plugin_path > /dev/null 2>&1
rm -rf /usr/lib/enigma2/python/Components/Converter/Wire* > /dev/null 2>&1

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

#check and install dependencies
#########################################
#Check image python
#########################################
#check python
python=$(python -c"from sys import version_info; print(version_info[0])")
case $python in 
3)
echo ""
sleep 3
;;
*)
echo "> your image python version: $python is not supported"
sleep 3
exit 1
;;
esac

#check and install dependencies
#########################################
deps=("wireguard-tools" "wireguard-tools-bash-completion" "openresolv" "python3-requests" "iptables")
left=">>>>"
right="<<<<"
LINE1="---------------------------------------------------------"
LINE2="-------------------------------------------------------------------------------------"
if [ -f /etc/opkg/opkg.conf ]; then
  STATUS='/var/lib/opkg/status'
  OSTYPE='Opensource'
  OPKG='opkg update'
  OPKGINSTAL='opkg install'
elif [ -f /etc/apt/apt.conf ]; then
  STATUS='/var/lib/dpkg/status'
  OSTYPE='DreamOS'
  OPKG='apt-get update'
  OPKGINSTAL='apt-get install -y'
fi
install() {
  if ! grep -qs "Package: $1" "$STATUS"; then
    $OPKG >/dev/null 2>&1
    rm -rf /run/opkg.lock
    echo -e "> Need to install ${left} $1 ${right} please wait..."
    echo "$LINE2"
    sleep 0.8
    echo
    if [ "$OSTYPE" = "Opensource" ]; then
      $OPKGINSTAL "$1"
      sleep 1
      clear
    elif [ "$OSTYPE" = "DreamOS" ]; then
      $OPKGINSTAL "$1" -y
      sleep 1
      clear
    fi
  fi
}
for i in "${deps[@]}"; do
  install "$i"
done

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
}
cleanup
    