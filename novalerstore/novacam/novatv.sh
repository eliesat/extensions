#!/bin/sh
if [ -d /usr/lib/enigma2/python/Plugins/Extensions/NovaTV ]; then
echo "> removing package please wait..."
sleep 3
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/NovaTV > /dev/null 2>&1

status='/var/lib/opkg/status'
package='enigma2-plugin-extensions-novatv'

if grep -q $package $status; then
opkg remove $package
fi

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3

else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

# Check python
py=$(python -c"from sys import version_info; print(version_info[0])")

echo "> checking dependencies please wait..."
sleep 1

if [ "$py" = 3 ]; then
for i in enigma2-plugin-systemplugins-serviceapp exteplayer3 ffmpeg gstplayer libc6 python3-core python3-cryptography python3-futures3 python3-image python3-json python3-multiprocessing python3-pillow python3-requests
do
opkg install $i >/dev/null 2>&1
done
else
for i in enigma2-plugin-systemplugins-serviceapp exteplayer3 ffmpeg gstplayer libc6 python-core python3-cryptography python-futures python-image python-json python-multiprocessing python-pillow python-requests
do
opkg install $i >/dev/null 2>&1
done
fi

#config
pack=novatv
version=11.0-r0
install="opkg install --force-reinstall"
#check python version
python=$(python -c "import platform; print(platform.python_version())")

case $python in 
2.7.18)
url='https://gitlab.com/eliesat/extensions/-/raw/main/novalerstore/novacam'
ipk="$pack-py2-$version.ipk"
;;
3.9.7|3.9.9)
url='https://gitlab.com/eliesat/extensions/-/raw/main/novalerstore/novacam'
ipk="$pack-py3.9-$version.ipk"
;;

3.11.0|3.11.1|3.11.2|3.11.3|3.11.5|3.11.5|3.11.6)
url='https://gitlab.com/eliesat/extensions/-/raw/main/novalerstore/novacam'
ipk="$pack-py3.11-$version.ipk"
;;
3.12.*)
url='https://gitlab.com/eliesat/extensions/-/raw/main/novalerstore/novacam'
ipk="$pack-py3.12-$version.ipk"
;;
*)
echo "> your image python version: $python is not supported"
sleep 3
exit 1
;;
esac


# Download and install plugin
echo "> Downloading "$pack"-"$version" please wait..."
sleep 3

cd /tmp
set -e
wget --show-progress "$url/$ipk"
$install $ipk
set +e
install=$?
cd ..
wait
rm -f /tmp/$ipk

echo ''
if [ $install -eq 0 ]; then
echo "> $plugin-$version package installed successfully"
echo "> Uploaded By ElieSat"
sleep 3

else

echo "> $plugin-$version package installation failed"
sleep 3
fi

fi